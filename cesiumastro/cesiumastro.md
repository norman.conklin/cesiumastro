**Spirent Traffic and Application Testing traversing a DUT/SUT into an emulated network**
#

**Proof of Concept User Summary:**
 - User: Cesium Astro- Nikki Gilbert
 - Access Dates: 10/16/23 – 12/31/23
 - Access Times: Business Hours with John Schubert
 - Access Level: Full (user can create, configure & run tests)

**Description: Test & Measurement Capabilities**

In this testbed, we are emulating stateful traffic patterns, which are going to be subject to real world impairments (like jitter, delay, latency, etc.) while rendering them over network elements.
 
#
**POC Test criteria:**
 -	Connecting DUT/SUT, attaching to a network, verifying IP address assignment, and the DUT accepts the traffic as sent
 -	Passing IP traffic end-to-end without impairments, and documenting baseline performance.  Verifying DUT processes and accepts traffic at rates specified from 0 to 100% line rate.
 -  Passing IP traffic end-to-end with impairments (bad, worse, and worst), and documenting performance related to customer requirements

**POC Milestones:**
 - Incorporating TBD custom traffic patterns with customer defined requirements


**Challenges:**
 - Testing a real DUT/SUT with numerous real-world traffic patterns
 - Keeping testing cycle short enough, yet yielding high test scenario execution


**Customer Benefits:**
 - Gain confidence that STC will deliver the expected tests


**Solution Elements:**
 - TestCenter



----
----

#### Content

| Links |
| ------ |
| [TestCenter](https://www.spirent.com/products/testcenter-ethernet-ip-cloud-test) |
